# ansible-tf

### Docker image for ARMv7 Processor rev 4 architecture running on Alpine linux with Ansible Core and Terraform installed (along with requirement packages and tools like: git, wget, curl)

- Alpine Linux v3.19

- ansible [core 2.16.1]

- ansible-galaxy [core 2.16.1]

- Terraform v1.8.1

## boilerplate example ansible playbook

In this example there is a playbook that creates a dedicated ansible user on the inventory rather than using root. The playbook configures the user's ssh-key, and sudoers configuration.

## usage

1. pull the Docker image (optionally you can build your own using Dockerfile)

```$ docker pull henrybravo/ansible-tf```

https://hub.docker.com/r/henrybravo/ansible-tf

2. clone this repository

```git clone https://gitlab.com/henrybravo/ansible-tf.git```

3. change directory

```cd ansible-tf```

4. create your ssh-keypair and place the keys into `ssh-keys` directory

```ssh-keygen ...```

5. run the create ansible user module

```docker run -it --rm -v $PWD/etc/ansible.cfg:/etc/ansible/ansible.cfg -v $PWD/:/ansible henrybravo/ansible-tf ansible-playbook $PWD/playbooks/create_users.yaml -i $PWD/inventory/ansible-hosts.yaml --become --ask-pass --ask-become-pass```

6. run tests using ping and command ansible core modules, for example:

```docker run -it --rm -v $PWD/etc/ansible.cfg:/etc/ansible/ansible.cfg -v $PWD/:/ansible henrybravo/ansible-tf ansible all -m ping```

```docker run -it --rm -v $PWD/etc/ansible.cfg:/etc/ansible/ansible.cfg -v $PWD/:/ansible henrybravo/ansible-tf ansible all -m command -a 'cat /etc/sudoers'```

```docker run -it --rm -v $PWD/etc/ansible.cfg:/etc/ansible/ansible.cfg -v $PWD/:/ansible henrybravo/ansible-tf ansible all -m command -a 'getent passwd'```

```docker run -it --rm -v $PWD/etc/ansible.cfg:/etc/ansible/ansible.cfg -v $PWD/:/ansible henrybravo/ansible-tf ansible all -m command -a 'getent shadow'```

```docker run -it --rm -v $PWD/etc/ansible.cfg:/etc/ansible/ansible.cfg -v $PWD/:/ansible henrybravo/ansible-tf ansible all -m command -a 'top bn-1'```