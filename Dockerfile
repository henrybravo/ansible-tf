FROM alpine
RUN apk update && \
    apk add --update --no-cache ansible wget curl git sshpass openssh && \
    wget -c https://releases.hashicorp.com/terraform/1.8.1/terraform_1.8.1_linux_arm.zip && \
    unzip terraform_1.8.1_linux_arm.zip && \
    mv terraform /usr/local/bin && \
    chmod +x /usr/local/bin/terraform && \
    rm -f terraform_1.8.1_linux_arm.zip
